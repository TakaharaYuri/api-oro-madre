'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { parse } = require('rss-to-json');
const NewsModel = use('App/Models/News');
const Event = use("Event");


/**
 * Resourceful controller for interacting with notices
 */
class NewsController {
  /**
   * Show a list of all notices.
   * GET notices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    var rss = await parse('https://cointelegraph.com.br/rss');
    if (rss.items) {

      for (let index = 0; index < rss.items.length; index++) {
        let column = {};
        const element = rss.items[index];
        column.category = JSON.stringify(rss.items[index].category);
        column.enclosures = JSON.stringify(rss.items[index].enclosures);
        column.media = JSON.stringify(rss.items[index].media);
        column.ref = rss.items[index].published;
        column.title = rss.items[index].title;
        column.description = rss.items[index].description;
        column.author = rss.items[index].author;
        column.published = rss.items[index].published;
        column.created = rss.items[index].created;
        column.link = rss.items[index].link;

        const check = await NewsModel.query().where('ref', column.ref).first();

        if (!check) {
          await NewsModel.create(column);
          await this.sleep(5000);
          Event.fire("new:sendNewsTelegram", column);

        }

      }
    }

    return rss.items;
  }

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

module.exports = NewsController
