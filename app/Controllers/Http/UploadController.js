"use strict";
const cloudinary = use("cloudinary");
const Env = use("Env");
const Helpers = use("Helpers");

cloudinary.config({

  cloud_name: Env.get('CLOUDINARY_CLOUD_NAME'),
  api_key: Env.get('CLOUDINARY_API_KEY'),
  api_secret: Env.get('CLOUDINARY_API_SECRET'),
})

class UploadController {
  async uploadImage({ request, response }) {
    const image = request.file("image", {
      size: "25mb",
    });

    console.log('TYPE ->', image.type);

    const cloudinaryReturn = await cloudinary.v2.uploader.upload(image.tmpPath, {folder:'analysis', resource_type: image.type });
    return  response.json({
      fileName:cloudinaryReturn.secure_url,
      data:cloudinaryReturn
    }); 

    // await image.move(Helpers.publicPath("uploads"), {
    //   name: `${new Date().getTime()}.${image.subtype}`,
    // });

    // return image;
  }

  async showFile({ response, params }) {
    return response.download(Helpers.publicPath(`uploads/${params.file}`));
  }
}

module.exports = UploadController;
