'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with auths
 */

const model = use('App/Models/User');
const PasswordResetModel = use('App/Models/PasswordReset');
// const UserDataModel = use('App/Models/Manager/UserData');
const moment = use('moment');
const Event = use("Event");
const Encryption = use("Encryption");
const { v4: uuidv4 } = require('uuid');
class AuthController {
  /**
   * Show a list of all auths.
   * GET auths
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async register({ request, response }) {
    const data = request.post();
    // const query = await model.create(data);
    return false;
  }

  async login({ request, response, auth }) {
    let data = {};
    const { email, password } = request.all();

    if (await auth.attempt(email, password)) {
      let user = await model.query()
        .with('user_data')
        .with('plan')
        .whereNotNull('current_period_end')
        .where('email', email).first();

      if (user) {
        if (user.status > 0) {
          const token = await auth.generate(user);
          const current_date = moment(new Date());
          let end_days;
          if (user.trial_enabled === 1) {
            end_days = moment(user.trial_days_ended);
            user.hours_left = end_days.diff(current_date, 'hours');
          }

          if (user.trial_enabled === 0) {
            if (user.current_period_end >= new Date()) {

              data.result = true;
              data.token = token;
              data.user = user.toJSON();

              return response.json(data);
            }

            else {
              user.status = 3;
              await user.save();
              return response.json({
                result: false,
                message: 'Sua conta ainda não está ativa por falta de pagamento.'
              })
            }
          }

          else if (user.hours_left > 0) {
            data.result = true;
            data.token = token;
            data.user = user.toJSON();

            return response.json(data);
          }
          else {
            user.status = 4;
            await user.save();
            return response.json({
              result: false,
              message: 'Seu período de teste terminou, para continuar utilizando.'
            })
          }
        }
        else {
          return response.json({
            result: false,
            message: 'Ocorreu um problema ao efetuar login, entre em contato com o suporte.'
          })
        }
        // if (user.status == 2) {
        //   return response.json({
        //     result: false,
        //     message: 'Sua conta ainda não está ativa, é necessário aguardar até que o pagamento seja efetivado'
        //   })
        // }
        // if (user.status == 3) {
        //   return response.json({
        //     result: false,
        //     message: 'Sua conta ainda não está ativa por falta de pagamento.'
        //   })
        // }
      }
      else {
        return response.json({
          result: false,
          message: 'Não foi possível fazer login na sua conta, entre em contato com suporte'
        })
      }
    }
    else {
      return response.status(403).json({
        result: false,
        message: 'Não foi possível encontrar seu usuário, verifique seu e-mail e tente novamente.'
      })
    }
  }

  async validate({ request, response }) {
    const { field, value } = request.get();

    const query = await model.query().where(field, value).first();
    if (query) {
      return response.json({
        result: true,
        message: 'Dados informados já existem'
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Dados não encontrados'
      })
    }
  }

  async resetPassword({ request, response }) {
    const { email } = request.post();

    const user = await model.query().where('email', email).whereNotIn('status', [99]).first();
    if (user) {
      let validUntil = new Date();
      validUntil.setMinutes(validUntil.getMinutes() + 1440);
      validUntil = new Date(validUntil);
      console.log(validUntil)

      const reset = {
        key: uuidv4(),
        user_id: user.id,
        valid_until: validUntil
      }

      await PasswordResetModel.query().where('user_id', user.id).delete();
      const query = await PasswordResetModel.create(reset);

      if (query) {
        Event.fire("new:RequestPassword", user.id, reset.key);
        return response.json({
          result: true,
          message: `Um e-mail foi enviado para ${user.email} verifique sua caixa de entrada e siga os passos descritos no e-mail`
        });
      }
      else {
        return response.json({
          result: false,
          message: 'Ocorreu um problema ao solicitar a criação de uma nova senha'
        })
      }
      // const emailQueue = {
      //   type:'reset-password',
      //   model:'reset-passowrd',
      //   from:'no-reply@oromadre.co',
      //   to:'',
      //   subject:'🔐 Solicitação de Nova Senha | OroMadre'
      // }
      // const emailQueueQuery = await EmailQueueModel.create()
    }
    else {
      return response.json({
        result: false,
        message: 'Não conseguimos localizar sua conta, em caso de dúvida entre em contato com o suporte'
      });
    }
  }

  async checkPasswordValidade({ params, response }) {
    const { key } = params;
    const resetValidate = await PasswordResetModel.query().where('key', key).first();
    const now = moment(new Date());

    if (resetValidate) {
      const validUntil = moment(resetValidate.valid_until);
      const end = validUntil.diff(now, 'hours');

      if (end > 0) {
        const user = await model.find(resetValidate.user_id);
        return response.json({
          result: true,
          key: Encryption.encrypt(resetValidate.user_id)
        })
      }
      else {
        return response.json({
          result: false,
          message: 'Este link não é mais válido, solicite ou um novo link ou tente novamente mais tarde'
        });
      }

    }
    else {
      return response.json({
        result: false,
        message: 'Este link não é mais válido, solicite ou um novo link ou tente novamente mais tarde'
      });
    }
  }


  async createPassword({ request, response }) {
    const { key, password } = request.post();
    const user = await model.query().where('id', Encryption.decrypt(key)).first();
    user.password = password;
    user.merge({ password: password });
    const query = await user.save();

    await PasswordResetModel.query().where('user_id', user.id).delete();

    if (query) {
      return response.json({
        result: true,
        message: 'Pronto, sua senha foi alterada com sucesso. Você será redirecionado para o login.'
      });
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um problema ao salvar sua senha, tente novamente mais tarde.'
      });
    }
  }


}

module.exports = AuthController
