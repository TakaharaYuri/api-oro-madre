'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pagarmes
 */
const pagarme = use('pagarme');
const Env = use('Env')
const PAGARME_ENCRYPTION_KEY = Env.get('PAGARME_ENCRYPTION_KEY');
const PAGARME_API_KEY = Env.get('PAGARME_API_KEY');
const UserModel = use('App/Models/User');
const UserDataModel = use('App/Models/Manager/UserData');
const UserPaymentHistoryModel = use('App/Models/Manager/UserPaymentHistory');
const PlanModel = use('App/Models/Manager/Plan');
const moment = use('moment');
const Event = use("Event");


class PagarmeController {
  /**
   * Show a list of all pagarmes.
   * GET pagarmes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async process({ request, response }) {
    const dados = request.only(['address', 'accept_terms', 'bith_date', 'cpf', 'email', 'gender', 'name', 'phone', 'password']);
    const creditCard = request.only(['card_number', 'card_holder_name', 'card_expiration_date', 'card_cvv']);
    const { plan_id } = request.post();
    
    const checkUsersExists = await UserModel.query().where('cpf', dados.cpf).first();
    const plan = await PlanModel.query().where('id', plan_id).first();
    let user = {};
    let paymentHistory = {};
    let userData = {};

    if (!checkUsersExists) {

      var today = new Date();
      var trial_ended = today;
      const end_days = moment(dados.trial_days_ended);
      const current_date = moment(new Date());

      trial_ended.setDate(today.getDate() + 3);
      dados.address = JSON.stringify(dados.address);
      dados.trial_days_ended = today;
      dados.hours_left = end_days.diff(current_date, 'hours');
      dados.trial_enabled = true;

      user = await UserModel.create(dados);
      user.address = JSON.parse(user.address);
    }
    else {
      user = checkUsersExists;
      user.address = JSON.parse(user.address);
    }

    // const userCreate = await 

    const cardValidators = pagarme.validate({ card: creditCard });

    if (cardValidators) {
      if (cardValidators.card.card_holder_name == false) {
        return response.json({
          result: false,
          message: 'O nome informado no cartão está incorreto'
        })
      }
      if (cardValidators.card.card_number == false) {
        return response.json({
          result: false,
          message: 'O número do cartão informado é inválido'
        })
      }
      if (cardValidators.card.card_expiration_date == false) {
        return response.json({
          result: false,
          message: 'A data de vencimento do seu cartão é inválida'
        })
      }
      if (cardValidators.card.card_cvv == false) {
        return response.json({
          result: false,
          message: 'O (CVV) do seu cartão é inválido'
        })
      }
    }


    let pagarmeClient = await pagarme.client.connect({ encryption_key: PAGARME_ENCRYPTION_KEY })
    const cardHash = await pagarmeClient.security.encrypt(creditCard);
    const splitedPhone = user.phone.split('');
    const _ddd = splitedPhone[0] + splitedPhone[1];
    splitedPhone.splice(0, 2);
    const _phone = splitedPhone.join("");

    const normalization = {
      payment_method: 'credit_card',
      card_hash: cardHash,
      plan_id: plan.pagarme_plan_id,
      // postback_url: `https://env99rwritz6crk.m.pipedream.net?user_id=${user.id}`,
      postback_url: `${Env.get('PROD_URL')}/api/payment-gateway/pagarme/postback?user_id=${user.id}`,
      customer: {
        name: user.name,
        email: user.email,
        document_number: user.cpf,
        phone: {
          ddd: _ddd,
          number: _phone

        },
        address: user.address
      }
    }


    pagarmeClient = await pagarme.client.connect({ api_key: PAGARME_API_KEY });
    user.address = JSON.stringify(user.address);

    try {


      const pagarmeRequest = await pagarmeClient.subscriptions.create(normalization);

      if (pagarmeRequest.status === 'paid' || pagarmeRequest.status === 'trialing') {

        user.plan_situation = pagarmeRequest.status;
        user.current_period_start = pagarmeRequest.current_period_start;
        user.current_period_end = pagarmeRequest.current_period_end;
        user.card_hash = cardHash;
        user.card_brand = pagarmeRequest.card_brand;
        user.card_last_digits = pagarmeRequest.card_last_digits;
        user.status = 1;
        user.pagarme_metadata = JSON.stringify(pagarmeRequest);

        await user.save();

        userData.user_id = user.id;
        userData.last_login = new Date();
        await UserDataModel.create(userData);

        paymentHistory.price = Number(pagarmeRequest.plan.amount) / 100;
        paymentHistory.gateway_data = JSON.stringify(pagarmeRequest);
        paymentHistory.plan_id = plan.id;
        paymentHistory.user_id = user.id;
        paymentHistory.method = pagarmeRequest.payment_method;

        console.log(paymentHistory);

        await UserPaymentHistoryModel.create(paymentHistory);


        Event.fire("new:Register", user.id);

        return response.json({
          result: true,
          data: pagarmeRequest,
          message: 'Pagamento realizado com sucesso. Você será redirecionado para a tela de login para acessar a plataforma'
        })
      }
      else {
        user.plan_situation = 'unpaid';
        user.status = 2;
        await user.save();

        return response.json({
          result: false,
          data: pagarmeRequest.response,
          message: 'Ocorreu um problema ao processar sua requisição',
          meta_data: pagarmeRequest
        })
      }
    } catch (error) {

      user.plan_situation = 'error';
      user.status = 2;
      await user.save();

      return response.json({
        result: false,
        response: error.response,
        message: error.response.errors[0].message,
        error: error
      })
    }

  }

  async webHookPostback({ request, params }) {
    console.log('Iniciando o Hook');
    let data = request.post();
    const { user_id } = request.get();

    let user = await UserModel.query().with('plan').where('id', user_id).first();
    user = user.toJSON();
    console.log(JSON.stringify(data));

    if (user) {
      user.plan_situation = data.current_status;
      if (user.plan_situation === 'canceled') {
        user.status = 3;
      }

      user.pagarme_metadata = JSON.stringify(data);

      const paymentHistory = {
        user_id: user.id,
        method: data.subscription.payment_method,
        price: Number(data.subscription.plan.amount) / 100,
        plan_id: user.plan.id,
        gateway_data: JSON.stringify(data)
      };

      delete user.plan;
      await UserPaymentHistoryModel.create(paymentHistory);
      let queryUser = await UserModel.query().where('id', user.id).first();
      queryUser.merge(user);
      await queryUser.save();
    }

    return data;
  }
}

module.exports = PagarmeController
