'use strict'

const User = require('../../../Models/User');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pagarmeintegrations
 */
const pagarme = use('pagarme');
const Env = use('Env')
const PAGARME_API_KEY = Env.get('PAGARME_API_KEY');
const UserModel = use('App/Models/User');
const UserPaymentHistoryModel = use('App/Models/Manager/UserPaymentHistory');
const PlanModel = use('App/Models/Manager/Plan');

class PagarmeIntegrationController {
  /**
   * Show a list of all pagarmeintegrations.
   * GET pagarmeintegrations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async getSubscription({ params, response, auth }) {
    let user = auth.user;
    if (user.pagarme_metadata) {
      user.pagarme_metadata = JSON.parse(user.pagarme_metadata);
    }

    const _pagarme = await pagarme.client.connect({ api_key: PAGARME_API_KEY });

    try {
      let data = await _pagarme.subscriptions.find({ id: user.pagarme_metadata.id });
      data.plan.amount = Number(data.plan.amount / 100);
      return data;

    } catch (error) {
      return error;
    }
  }

  async cancelSubscription({ response, auth }) {
    let user = await UserModel.query().where('id', auth.user.id).first();

    user = user.toJSON();

    if (user.pagarme_metadata) {
      user.pagarme_metadata = JSON.parse(user.pagarme_metadata);
    }

    const _pagarme = await pagarme.client.connect({ api_key: PAGARME_API_KEY });

    try {
      let data = await _pagarme.subscriptions.cancel({ id: user.pagarme_metadata.id });
      user.pagarme_metadata = JSON.stringify(data);

      let queryUser = await UserModel.query().where('id', user.id).first();
      queryUser.merge(user);
      await queryUser.save();

      if (data) {
        return {
          result: true,
          data: data
        }
      }

    } catch (error) {

      return {
        result: false,
        data: error
      };
    }
  }

}

module.exports = PagarmeIntegrationController
