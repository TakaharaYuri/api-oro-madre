'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const model = use('App/Models/User');

/**
 * Resourceful controller for interacting with clients
 */
class ClientController {
  /**
   * Show a list of all clients.
   * GET clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, auth }) {
    const { page, name = '', email = '', created_at = '', plan_situation = '' } = request.get();
    let query = model.query().with('plan').where('type', 'client');

    if (name != '') {
      query = query.where('name', 'LIKE', `%${name}%`);
    }

    if (email != '') {
      query = query.where('email', 'LIKE', `%${email}%`);
    }

    if (created_at != '') {
      query = query.where('created_at', created_at);
    }

    if (plan_situation != '') {
      query = query.where('plan_situation', plan_situation);
    }

    query = query.orderBy('updated_at', 'DESC').paginate(page, 10);
    return query;
  }

  /**
   * Render a form to be used for creating a new client.
   * GET clients/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new client.
   * POST clients
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    data.address = (data.address) ? JSON.stringify(data.address) : {};
    const query = await model.create(data);

    if (query) {
      return response.json({
        result: true,
        data: query
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um problema ao criar o usuario'
      })
    }
  }

  /**
   * Display a single client.
   * GET clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response }) {
    const data = await model.query().with('plan').where('id', params.id).setHidden(['password']).first();
    if (data) {
      data.address = JSON.parse(data.address);
    }

    return data;
  }


  /**
   * Update client details.
   * PUT or PATCH clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a client with id.
   * DELETE clients/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const client = await model.findOrFail(params.id);
    return await client.delete();
  }
}

module.exports = ClientController
