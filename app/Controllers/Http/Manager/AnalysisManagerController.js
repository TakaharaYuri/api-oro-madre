'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with analysismanagers
 */

const model = use('App/Models/Analysis');
const Query = use('Query');
const modelLikes = use('App/Models/AnalysisLike');
const Event = use("Event");



class AnalysisManagerController {
  /**
   * Show a list of all analysismanagers.
   * GET analysismanagers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const { page, market_type = '', coin_pairs_id = '', user_id = '', created_at = '', operation_type = '' } = request.get();
    let query = model.query().with('coin_pair').with('user').where('status', 1)

    if (market_type != '') {
      query = query.where('market_type', market_type);
    }

    if (coin_pairs_id != '') {
      query = query.where('coin_pairs_id', coin_pairs_id);
    }

    if (user_id != '') {
      query = query.where('user_id', user_id);
    }

    if (created_at != '') {
      query = query.where('created_at', created_at);
    }

    if (operation_type != '') {
      query = query.where('operation_type', operation_type);
    }

    query = await query.orderBy('created_at', 'DESC').paginate(page, 10);

    query = query.toJSON();
    for (const item of query.data) {
      if (item.take_profit) item.take_profit = JSON.parse(item.take_profit);
    }

    return response.json(query);
  }

  async room({ request, response, auth }) {
    const { page, market_type = '', coin_pairs_id = '', user_id = '', created_at = '', operation_type = '', term = '' } = request.get();

    const querySearch = new Query(request, { order: 'id' });
    let query = model.query()
      .with('coin_pair')
      .with('user', (builder) => {
        builder.select(['name', 'email', 'photo', 'id'])
      })
      .where('status', 1);

    if (auth.user.plan_situation === 'paid') {

      if (term != '') {
        query = query.whereRaw(`((operation_type LIKE '%${term}%') OR (market_type LIKE '%${term}%') OR (leverage_type LIKE '%${term}%'))`)
      }

      if (market_type != '') {
        query = query.where('market_type', market_type);
      }

      if (coin_pairs_id != '') {
        query = query.where('coin_pairs_id', coin_pairs_id);
      }

      if (user_id != '') {
        query = query.where('user_id', user_id);
      }

      if (created_at != '') {
        query = query.where('created_at', created_at);
      }

      if (operation_type != '') {
        query = query.where('operation_type', operation_type);
      }

      query = await query.where(querySearch.search(['operation_type', 'market_type', 'leverage_type']))
        .orderBy('created_at', 'DESC')
        .paginate(page, 6);
    }
    else {
      query = await model.query()
        .with('coin_pair')
        .with('user', (builder) => {
          builder.select(['name', 'email', 'photo', 'id'])
        })
        .where('status', 1)
        .where(querySearch.search(['operation_type', 'market_type', 'leverage_type']))
        .orderBy('created_at', 'DESC')
        .paginate(page, 1);
    }

    query = query.toJSON();
    for (const item of query.data) {
      if (item.take_profit) item.take_profit = JSON.parse(item.take_profit);
    }

    return response.json(query);
  }

  /**
   * Create/save a new analysismanager.
   * POST analysismanagers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    let data = request.all();
    if (data.take_profit) {
      data.take_profit = JSON.stringify(data.take_profit);
    }

    if (data.images) data.images = JSON.stringify(data.images);

    const query = await model.create(data);

    Event.fire("new:sendToTelegram", query.id);

    return response.json(query);
  }

  /**
   * Display a single analysismanager.
   * GET analysismanagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, response }) {
    let query = await model.query().where('id', params.id).where('status', 1).first();

    if (query) {
      if (query.take_profit) query.take_profit = JSON.parse(query.take_profit);
      if (query.images) query.images = JSON.parse(query.images);

      return response.json(query);
    }
    else {
      return response.status(404).json({
        result: false,
        message: 'Nenhum registro encontrado com o parametro informado'
      })
    }
  }

  async detail({ params, response, auth }) {
    if (auth.user.plan_situation === 'paid') {
      let query = await model.query()
        .with('coin_pair')
        .with('user', (builder) => {
          builder.select(['name', 'email', 'photo', 'id'])
        })
        .where('key', params.id)
        .where('status', 1)
        .first();

      if (query) {
        query = query.toJSON();
        query.take_profit = (query.take_profit) ? JSON.parse(query.take_profit) : null;
        query.images = (query.images) ? JSON.parse(query.images) : null;

        const queryLike = await modelLikes.query().where('user_id', query.user.id).first();
        query.is_liked = (queryLike) ? true : false;

        if (query.is_liked) {
          query.like_id = queryLike.id;
        }
        return response.json({
          result: true,
          data: query
        })
      }
      else {
        return response.json({
          result: false,
          message: 'Não foi possível encontrar a análise solicitada.'
        })
      }
    }
    else {
      return response.json({
        result: false,
        message: 'Para ver mais detalhes desta Análise, verifique seu metódo de pagamento ou entre em contato com o suporte.'
      })
    }
  }


  /**
   * Update analysismanager details.
   * PUT or PATCH analysismanagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const analysis = await model.findOrFail(params.id);

    if (data.take_profit) data.take_profit = JSON.stringify(data.take_profit);
    if (data.images) data.images = JSON.stringify(data.images);

    analysis.merge(data);
    await analysis.save();
    return analysis;
  }

  /**
   * Delete a analysismanager with id.
   * DELETE analysismanagers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const marca = await model.findOrFail(params.id);
    return await marca.delete();
  }


  async sendToTelegram({ params, response }) {
    Event.fire("new:sendToTelegram", params.id);
    return {
      result: true
    }
  }

}

module.exports = AnalysisManagerController
