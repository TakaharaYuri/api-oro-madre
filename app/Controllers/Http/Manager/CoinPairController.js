'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with coinpairs
 */

const model = use('App/Models/Manager/CoinPair');

class CoinPairController {
  /**
   * Show a list of all coinpairs.
   * GET coinpairs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const data = await model.query().orderBy('created_at', 'desc').fetch();
    return data; 
  }


  /**
   * Create/save a new coinpair.
   * POST coinpairs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.post();
    const query = await model.create(data);
    return query;
  }


  /**
   * Update coinpair details.
   * PUT or PATCH coinpairs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const marca = await model.findOrFail(params.id);

    marca.merge(data);
    await marca.save();
    return marca;
  }

  /**
   * Delete a coinpair with id.
   * DELETE coinpairs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const marca = await model.findOrFail(params.id);
    return await marca.delete();
  }
}

module.exports = CoinPairController
