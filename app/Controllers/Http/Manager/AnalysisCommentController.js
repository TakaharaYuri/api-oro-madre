'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with analysiscomments
 */

const model = use('App/Models/AnalysisComment');
const userModel = use('App/Models/User');

class AnalysisCommentController {
  /**
   * Show a list of all analysiscomments.
   * GET analysiscomments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
  }

  /**
   * Create/save a new analysiscomment.
   * POST analysiscomments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    let data = request.post();
    data.user_id = auth.user.id;
    let query = await model.create(data);

    if (query) {
      query.user = await userModel.query().select('name', 'email', 'photo', 'type').where('id', query.user_id).first();
      return response.json({
        result: true,
        data: query
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um erro ao comentar esta análise.'
      })
    }
  }

  /**
   * Display a single analysiscomment.
   * GET analysiscomments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, response }) {
    let query = await model.query()
      .with('user', (builder) => {
        builder.select(['name', 'email', 'photo', 'id', 'type']).setHidden(['id'])
      })
      .where('analysis_id', params.id)
      .orderBy('created_at', 'DESC')
      .fetch();

    return query;
  }

  /**
   * Update analysiscomment details.
   * PUT or PATCH analysiscomments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response, auth }) {
    const data = request.post();
    let normalize = {
      id: data.id,
      comment: data.comment,
      edited:data.edited,
      parent_id: data.parent_id,
      user_id: data.user_id
    }
    if (auth.user.id == normalize.user_id) {

      const comment = await model.findOrFail(params.id);
      comment.merge(normalize);
      await comment.save();

      return response.json({
        result: true,
        data: comment
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um problema ao atualizar o seu comentário.'
      })
    }

  }

  /**
   * Delete a analysiscomment with id.
   * DELETE analysiscomments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const data = await model.findOrFail(params.id);
    let query = await data.delete();
    return response.json({
      result: query
    })
  }
}

module.exports = AnalysisCommentController
