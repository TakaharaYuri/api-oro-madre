'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with clientpayments
 */

const model = use('App/Models/Manager/UserPaymentHistory');

class ClientPaymentController {
  /**
   * Show a list of all clientpayments.
   * GET clientpayments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, params }) {
    const { user_id } = request.get();
    let data = await model.query().where('user_id', user_id).orderBy('created_at', 'desc').fetch();
    data = data.toJSON();
    data.map(item => {
      item.gateway_data = (item.gateway_data)? JSON.parse(item.gateway_data):null;
    })
    return data; 
  }

  /**
   * Render a form to be used for creating a new clientpayment.
   * GET clientpayments/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new clientpayment.
   * POST clientpayments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single clientpayment.
   * GET clientpayments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing clientpayment.
   * GET clientpayments/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update clientpayment details.
   * PUT or PATCH clientpayments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a clientpayment with id.
   * DELETE clientpayments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ClientPaymentController
