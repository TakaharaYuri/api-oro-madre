'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with plans
 */

const model = use('App/Models/Manager/Plan');

class PlanController {
  /**
   * Show a list of all plans.
   * GET plans
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    // Update
    const data = await model.query().orderBy('created_at', 'desc').fetch();
    return data;
  }


  /**
   * Create/save a new plan.
   * POST plans
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.post();
    const query = await model.create(data);
    return query;
  }

  /**
   * Display a single plan.
   * GET plans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response }) {
    let query = await model.query().where('status', 1).first();
    return query;
  }


  /**
   * Update plan details.
   * PUT or PATCH plans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const plan = await model.findOrFail(params.id);

    plan.merge(data);
    await plan.save();
    return plan;
  }

  /**
   * Delete a plan with id.
   * DELETE plans/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const plan = await model.findOrFail(params.id);
    return await plan.delete();
  }


  async getCupom({ params, request, response }) {
    const plan = await model.query().where('cupom', params.cupom).first();

    if (plan) {
      return response.json({
        result: true,
        data: plan
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Não foi possível encontrar o cupom informado'
      })
    }
  }
}

module.exports = PlanController
