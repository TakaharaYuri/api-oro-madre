'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
const moment = use('moment');
const model = use('App/Models/User');
const Env = use('Env')

class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response }) {
    const data = await model.query().where('type', 'admin').orderBy('created_at', 'desc').fetch();
    return data;
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    let data = request.post();
    data.address = (data.address)? JSON.stringify(data.address) : {};
    const query = await model.create(data);

    if (query) {
      return response.json({
        result: true,
        data: query
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um problema ao criar o usuario'
      })
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {

    const data = await model.query().where('id', params.id).setHidden(['password']).first();
    return data;
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const user = await model.findOrFail(params.id);

    user.merge(data);
    await user.save();
    return user;
  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }


  async getLoggedUser({ auth }) {
    let user = auth.user;
    user.address = JSON.parse(user.address);

    if (user.trial_enabled === 1) {
      const current_date = moment(new Date());
      let end_days = moment(user.trial_days_ended);
      user.hours_left = end_days.diff(current_date, 'hours');
    }

    delete user.password;
    return user;
  }

}

module.exports = UserController
