'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const model = use('App/Models/AnalysisLike');
const userModel = use('App/Models/User');
const analysisModel = use('App/Models/Analysis');
/**
 * Resourceful controller for interacting with analysislikes
 */
class AnalysisLikeController {
  /**
   * Show a list of all analysislikes.
   * GET analysislikes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  /**
   * Create/save a new analysislike.
   * POST analysislikes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {
    let data = request.post();
    data.user_id = auth.user.id;
    let query = await model.create(data);

    if (query) {
      query.user = await userModel.query().select('name', 'email', 'photo', 'type').where('id', query.user_id).first();
      const analysis = await analysisModel.query().where('id', data.analysis_id).first();
      analysis.likes_count++;
      await analysis.save();

      return response.json({
        result: true,
        data: query
      })
    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um erro ao comentar esta análise.'
      })
    }
  }


  /**
   * Update analysislike details.
   * PUT or PATCH analysislikes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a analysislike with id.
   * DELETE analysislikes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response, auth }) {
    let data = request.post();
    let query = await model.query().where('id', params.id).where('user_id', auth.user.id).first();
    if (query) {
      const analysis = await analysisModel.query().where('id', query.analysis_id).first();
      await query.delete();

      analysis.likes_count--;
      await analysis.save();

      return response.json({
        result: true,
        data: query
      })

    }
    else {
      return response.json({
        result: false,
        message: 'Ocorreu um erro ao comentar esta análise.'
      })
    }
  }
}

module.exports = AnalysisLikeController
