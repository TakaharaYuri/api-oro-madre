'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with cupons
 */

const model = use('App/Models/Manager/Cupon')

class CuponController {
  /**
   * Show a list of all cupons.
   * GET cupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
    const data = await model.query().orderBy('created_at', 'desc').fetch();
    return data;
  }


  /**
   * Create/save a new cupon.
   * POST cupons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const data = request.post();
    const query = await model.create(data);
    return query;
  }

  /**
   * Display a single cupon.
   * GET cupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }


  /**
   * Update cupon details.
   * PUT or PATCH cupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.post();
    const cupom = await model.findOrFail(params.id);

    cupom.merge(data);
    await cupom.save();
    return cupom;
  }

  /**
   * Delete a cupon with id.
   * DELETE cupons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const cupom = await model.findOrFail(params.id);
    return await cupom.delete();
  }
}

module.exports = CuponController
