'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const moment = use('moment');

class User extends Model {
  static boot() {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany('App/Models/Token')
  }

  user_data() {
    return this.hasOne('App/Models/Manager/UserData', 'id', 'user_id');
  }

  plan() {
    return this.hasOne('App/Models/Manager/Plan', 'plan_id', 'id')
  }

  static get dates() {
    return super.dates.concat(['current_period_start', 'current_period_end', 'birth_date']);
  }

  static castDates(field, value) {
    if (field.includes(['current_period_start', 'current_period_end'])) {
      return moment(value, "YYYY-MM-DD H:mm:ss").format("YYYY-MM-DD H:mm:ss");
    }
    return super.formatDates(field, value)
  }
}

module.exports = User
