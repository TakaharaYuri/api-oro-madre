'use strict'

const { v4: uuidv4 } = require('uuid');

const AnalysisHook = exports = module.exports = {}

AnalysisHook.uuid = async (modelInstance) => {
  modelInstance.key = uuidv4();
}
