'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Analysis extends Model {

  static boot() {
    super.boot();
    this.addHook("beforeCreate", "AnalysisHook.uuid");
  }

  static get table() {
    return 'analysis';
  }

  coin_pair () {
    return this.hasOne('App/Models/Manager/CoinPair', 'coin_pairs_id', 'id');
  }

  user() {
    return this.hasOne('App/Models/User', 'user_id', 'id');
  }
}

module.exports = Analysis
