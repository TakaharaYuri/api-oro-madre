'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserData extends Model {

  plan() {
    return this.hasOne('App/Models/Manager/Plan', 'plan_id', 'id');
  }

}

module.exports = UserData
