'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserPaymentHistory extends Model {
  static get table() {
    return 'user_payment_history';
  }
}

module.exports = UserPaymentHistory
