'use strict'

const Task = use('Task')
const axios = require('axios').default;
const cheerio = require('cheerio');
const md5 = require('md5');
const NewsModel = use('App/Models/News');
const Event = use("Event");

class MoneyTimesScrapper
 extends Task {
  static get schedule() {
    return '0 */15 * * * *'
  }

  async handle() {

    const html = await axios.get('https://www.moneytimes.com.br/cryptotimes/');
    const $ = cheerio.load(html.data);
    const news = $('#ultimas-home > div');

    news.map(async (index, element) => {
      const data = {
        title: $(element).find('div > h2 > a').text(),
        media: $(element).find('figure > a > img').attr('src'),
        ref: md5($(element).find('div > div.news-item__category').text()),
        link: $(element).find('div > h2 > a').attr('href'),
        author: 'Money Times'
      }

      if (data.media) {
        const check = await NewsModel.query().where('ref', data.ref).first();
        if (!check) {
          await NewsModel.create(data);
          await this.sleep(5000);
          Event.fire("new:sendNewsTelegram", data);
        }
      }
    })
  }

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

module.exports = MoneyTimesScrapper

