'use strict'

const Task = use('Task');
const Event = use("Event");
const { parse } = require('rss-to-json');
const NewsModel = use('App/Models/News');

class News extends Task {
  static get schedule() {
    return '0 */15 * * * *'
  }

  async handle() {
    var rss = await parse('https://cointelegraph.com.br/rss');
    if (rss.items) {

      for (let index = 0; index < rss.items.length; index++) {
        let column = {};
        const element = rss.items[index];
        column.category = JSON.stringify(rss.items[index].category);
        column.enclosures = JSON.stringify(rss.items[index].enclosures);
        column.media = rss.items[index].media.thumbnail.url;
        column.ref = rss.items[index].published;
        column.title = rss.items[index].title;
        column.description = rss.items[index].description;
        column.author = rss.items[index].author;
        column.published = rss.items[index].published;
        column.created = rss.items[index].created;
        column.link = rss.items[index].link;

        const check = await NewsModel.query().where('ref', column.ref).first();

        if (!check) {
          await NewsModel.create(column);
          await this.sleep(5000);
          Event.fire("new:sendNewsTelegram", column);

        }

      }
    }

    return rss.items;
  }

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

module.exports = News
