'use strict'

const cloudinary = use('cloudinary')
const Env = use('Env')

cloudinary.config({

    cloud_name: Env.get('CLOUDINARY_CLOUD_NAME'),
    api_key: Env.get('CLOUDINARY_API_KEY'),
    api_secret: Env.get('CLOUDINARY_API_SECRET'),
})

module.exports = {

    upload: async (file, type = "video") => {

        return new Promise(async (resolve, reject) => {

            try {

                let response = await cloudinary.uploader.upload(file, { folder: 'api', resource_type: type })

                resolve({ status: true, fileName: response.secure_url })

            } catch (error) {

                reject({ status: false, url: error.message })
            }
        })
    }
}