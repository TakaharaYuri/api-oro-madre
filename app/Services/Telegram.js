"use strict";
const axios = require('axios').default;
const API = 'https://api.telegram.org';
const Env = use('Env');

const API_KEY = Env.get('TELEGRAM_BOT_KEY');



module.exports = {
  sendMessage: async ({ message, chatId, }) => {
    return new Promise(async (resolve, reject) => {
      const options = {
        method: 'GET',
        url: `${API}/bot${API_KEY}/sendMessage?chat_id=${chatId}&text=${message}&parse_mode=markdown`
      }

      axios.request(options).then(
        (response) => {
          resolve(response)
        },
        (error) => {
          reject(error);
        }
      )
    })
  },

  sendPhoto: async ({ photo, chatId, message = '' }) => {
    return new Promise(async (resolve, reject) => {
      const options = {
        method: 'GET',
        url: `${API}/bot${API_KEY}/sendPhoto?chat_id=${chatId}&photo=${photo}&parse_mode=markdown&caption=${message}`
      }

      axios.request(options).then(
        (response) => {
          resolve(response)
        },
        (error) => {
          reject(error);
        }
      )
    })
  }
}