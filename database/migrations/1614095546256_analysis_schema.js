'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.string('risk_return');
    })
  }

  down () {
    this.table('analysis', (table) => {
      table.dropColumn('risk_return');
      // reverse alternations
    })
  }
}

module.exports = AnalysisSchema
