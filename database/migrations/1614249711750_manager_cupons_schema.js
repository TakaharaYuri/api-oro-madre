'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CuponsSchema extends Schema {
  up () {
    this.create('cupons', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.boolean('status').defaultTo(1)
      table.json('meta_data')
      table.string('discount_type').defaultTo('percentage')
      table.decimal('value', [10,2])
      table.integer('limit')
    })
  }

  down () {
    this.drop('cupons')
  }
}

module.exports = CuponsSchema
