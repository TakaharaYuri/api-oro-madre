'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.integer('hours_left').defaultTo(null);
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('hours_left');
    })
  }
}

module.exports = UsersSchema
