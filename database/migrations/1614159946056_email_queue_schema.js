'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmailQueueSchema extends Schema {
  up () {
    this.create('email_queues', (table) => {
      table.increments()
      table.timestamps()
      table.timestamp('send_at')
      table.string('status').defaultTo('queued')
      table.string('type')
      table.json('log')
      table.text('body')
      table.string('model')
      table.string('to')
      table.string('from')
      table.string('from_name')
      table.string('subject')
    })
  }

  down () {
    this.drop('email_queues')
  }
}

module.exports = EmailQueueSchema
