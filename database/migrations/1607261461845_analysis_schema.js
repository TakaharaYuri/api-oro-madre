'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.dropColumn('leverage_type');
      table.dropColumn('enter_price_type');
      table.dropColumn('stop_loss_type');
      table.varchar('operation_price_type');
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AnalysisSchema
