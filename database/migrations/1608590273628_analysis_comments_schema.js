'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisCommentsSchema extends Schema {
  up () {
    this.create('analysis_comments', (table) => {
      table.increments()
      table.timestamps()
      table.text('comment')
      table.integer('parent_id').defaultTo(null)
      table.boolean('edited').defaultTo(false)
      table
        .integer('analysis_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('analysis')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down () {
    this.drop('analysis_comments')
  }
}

module.exports = AnalysisCommentsSchema
