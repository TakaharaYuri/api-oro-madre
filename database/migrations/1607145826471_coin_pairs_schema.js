'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CoinPairsSchema extends Schema {
  up () {
    this.create('coin_pairs', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.boolean('status')
    })
  }

  down () {
    this.drop('coin_pairs')
  }
}

module.exports = CoinPairsSchema
