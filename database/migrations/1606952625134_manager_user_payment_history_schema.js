'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserPaymentHistorySchema extends Schema {
  up() {
    this.create('user_payment_history', (table) => {
      table.increments()
      table.timestamps()
      table.decimal('price', [10, 2])
      table.string('method')
      table.json('meta_data')
      table.boolean('status').defaultTo(true)
      table.json('gateway_data')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .integer('plan_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('plans')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down() {
    this.drop('user_payment_history')
  }
}

module.exports = UserPaymentHistorySchema
