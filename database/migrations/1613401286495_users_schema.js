'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.datetime('trial_days_ended');
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('trial_days_ended')
    })
  }
}

module.exports = UsersSchema
