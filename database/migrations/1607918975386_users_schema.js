'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.dropColumn('end_plan_date')
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.datetime('end_plan_date')
    })
  }
}

module.exports = UsersSchema
