'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NewsSchema extends Schema {
  up () {
    this.create('news', (table) => {
      table.increments()
      table.timestamps()
      table.string('title')
      table.text('description')
      table.string('link')
      table.string('author')
      table.string('published')
      table.string('created')
      table.string('ref')
      table.json('category')
      table.json('enclosures')
      table.json('media')
      table.boolean('send').defaultTo(false)
    })
  }

  down () {
    this.drop('news')
  }
}

module.exports = NewsSchema
