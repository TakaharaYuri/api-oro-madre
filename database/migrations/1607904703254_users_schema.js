'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.datetime('current_period_start');
      table.datetime('current_period_end');
      table.text('card_hash');
      table.string('card_brand');
      table.string('card_last_digits');
      // alter table
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('current_period_start');
      table.dropColumn('current_period_end');
      table.dropColumn('card_hash');
      table.dropColumn('card_brand');
      table.dropColumn('card_last_digits');
    })
  }
}

module.exports = UsersSchema
