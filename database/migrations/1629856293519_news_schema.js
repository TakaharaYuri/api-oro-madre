'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NewsSchema extends Schema {
  up () {
    this.table('news', (table) => {
      // alter table
      table.text('media')
      table.text('title')
    })
  }

  down () {
    this.table('news', (table) => {
      // reverse alternations
      table.dropColumn('media')
      table.dropColumn('title')
    })
  }
}

module.exports = NewsSchema
