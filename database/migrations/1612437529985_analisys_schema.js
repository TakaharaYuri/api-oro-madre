'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalisysSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.string('analysis_study_type').defaultTo('image')
      table.string('analysis_study_video')
    })
  }

  down () {
    this.table('analisys', (table) => {
      // reverse alternations
      table.dropColumn('analysis_study_type')
      table.dropColumn('analysis_study_video')
    })
  }
}

module.exports = AnalisysSchema
