'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlansSchema extends Schema {
  up () {
    this.table('plans', (table) => {
      // alter table
      table.string('pagarme_plan_id').defaultTo(null)
    })
  }

  down () {
    this.table('plans', (table) => {
      // reverse alternations
      table.dropColumn('pagarme_plan_id');
    })
  }
}

module.exports = PlansSchema
