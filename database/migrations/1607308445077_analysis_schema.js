'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.dropColumn('laverage_type');
      table.string('leverage_type');
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AnalysisSchema
