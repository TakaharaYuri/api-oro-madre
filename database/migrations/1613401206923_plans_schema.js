'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlansSchema extends Schema {
  up () {
    this.table('plans', (table) => {
      // alter table
      table.integer('trial_days').defaultTo('3')
    })
  }

  down () {
    this.table('plans', (table) => {
      // reverse alternations
      table.dropColumn('trial_days')
    })
  }
}

module.exports = PlansSchema
