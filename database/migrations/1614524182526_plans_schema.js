'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlansSchema extends Schema {
  up () {
    this.table('plans', (table) => {
      // alter table
      table.string('cupom')
    })
  }

  down () {
    this.table('plans', (table) => {
      // reverse alternations
      table.dropColumn('cupom')
    })
  }
}

module.exports = PlansSchema
