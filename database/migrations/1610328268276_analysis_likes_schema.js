'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisLikesSchema extends Schema {
  up () {
    this.create('analysis_likes', (table) => {
      table.increments()
      table.timestamps()
      table
        .integer('analysis_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('analysis')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down () {
    this.drop('analysis_likes')
  }
}

module.exports = AnalysisLikesSchema
