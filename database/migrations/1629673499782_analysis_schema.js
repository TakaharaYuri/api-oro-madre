'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.json('images')
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
      table.dropColumn('images')
    })
  }
}

module.exports = AnalysisSchema
