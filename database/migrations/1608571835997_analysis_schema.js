'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      table.uuid("key");
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AnalysisSchema
