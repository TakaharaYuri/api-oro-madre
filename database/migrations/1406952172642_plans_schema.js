'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlansSchema extends Schema {
  up () {
    this.create('plans', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.boolean('status').defaultTo(true)
      table.decimal('price', [10,2]);
      table.text('description')
    })
  }

  down () {
    this.drop('plans')
  }
}

module.exports = PlansSchema
