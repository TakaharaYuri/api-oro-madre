'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.string('analysis_permission').defaultTo('private');
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
      table.dropColumn('analysis_permission');
    })
  }
}

module.exports = AnalysisSchema
