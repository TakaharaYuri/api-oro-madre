'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserDataSchema extends Schema {
  up () {
    this.create('user_data', (table) => {
      table.increments()
      table.timestamps()
      table.boolean('use_whatsapp').defaultTo(false)
      table.boolean('use_telegram').defaultTo(false)
      table.datetime('last_login')
      table.json('meta_data')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
    })
  }

  down () {
    this.drop('user_data')
  }
}

module.exports = UserDataSchema
