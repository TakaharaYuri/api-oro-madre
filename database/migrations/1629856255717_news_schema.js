'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NewsSchema extends Schema {
  up () {
    this.table('news', (table) => {
      // alter table
      table.dropColumn('media');
      table.dropColumn('title');
    })
  }

  down () {
    this.table('news', (table) => {
      // reverse alternations
      table.json('media');
      table.string('title');
    })
  }
}

module.exports = NewsSchema
