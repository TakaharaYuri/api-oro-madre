'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.table('analysis', (table) => {
      // alter table
      table.integer('likes_count').defaultTo(0);
      table.integer('comments_count').defaultTo(0);
      table.integer('accepts_count').defaultTo(0);
      table.integer('simulation_count').defaultTo(0);
    })
  }

  down () {
    this.table('analysis', (table) => {
      // reverse alternations
      table.dropColumn('likes_count');
      table.dropColumn('comments_count');
      table.dropColumn('accepts_count');
      table.dropColumn('simulation_count');
    })
  }
}

module.exports = AnalysisSchema
