'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.boolean('trial_enabled').defaultTo(false);
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('trial_enabled');
    })
  }
}

module.exports = UsersSchema
