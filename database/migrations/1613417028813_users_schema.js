'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.json('pagarme_metadata');
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('pagarme_metadata');
    })
  }
}

module.exports = UsersSchema
