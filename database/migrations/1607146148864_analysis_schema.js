'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AnalysisSchema extends Schema {
  up () {
    this.create('analysis', (table) => {
      table.increments()
      table.timestamps()
      table.string('leverage')
      table.string('leverage_type')
      table.integer('enter_price_in')
      table.integer('enter_price_out')
      table.string('enter_price_type')
      table.integer('stop_loss')
      table.string('stop_loss_type')
      table.decimal('investiment_risk_initial')
      table.decimal('investiment_risk_medium')
      table.decimal('investiment_risk_advance')
      table.string('analysis_study_image')
      table.string('analysis_study_document')
      table.json('take_profit')
      table.json('meta_data')
      table.boolean('status').defaultTo(true)
      table.string('market_type')
      table.string('operation_type')
      table.text('summary')
      table.text('considerations')
      table
        .integer('coin_pairs_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('coin_pairs')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE')
    })
  }

  down () {
    this.drop('analysis')
  }
}

module.exports = AnalysisSchema
