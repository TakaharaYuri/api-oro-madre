'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.boolean('accept_terms').defaultTo(false)
      table.dropColumn('username');
      table.string('plan_situation').defaultTo('unpaid');
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
      table.dropColumn('accept_terms')
      table.string('username');
      table.dropColumn('plan_situation');
    })
  }
}

module.exports = UsersSchema
