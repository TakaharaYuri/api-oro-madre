'use strict'

/** @type {import('@adonisjs/framework/src/Event')} */
const Event = use("Event");
const UserModel = use('App/Models/User');
const analisysModel = use('App/Models/Analysis');
const coinPairModel = use('App/Models/Manager/CoinPair');
const ENV = use('Env');
const Mail = use("Mail");
const Encryption = use("Encryption");
const TelegramService = use('App/Services/Telegram');

const TELEGRAM_VIP_GROUP_ID = ENV.get('TELEGRAM_VIP_GROUP_ID');
const TELEGRAM_FREE_GROUP_ID = ENV.get('TELEGRAM_FREE_GROUP_ID');
const TELEGRAM_NEWS_GROUP_ID = ENV.get('TELEGRAM_NEWS_GROUP_ID');




Event.on('new:RequestPassword', async (user_id, uuid) => {
  console.log('[EVENTO] -> RequestPassword', user_id);
  const user = await UserModel.find(user_id);
  let link;

  if (ENV.get('NODE_ENV') === 'development') {
    link = `http://localhost:4200/#/forgot-password/reset/${uuid}`;
  }
  else {
    link = `https://painel.oromadre.co/#/forgot-password/reset/${uuid}`;
  }

  let email = {
    user: user,
    link: link
  }

  try {
    await Mail.send('emails/reset-password', { data: email }, (message) => {
      message
        .to(user.email)
        .from('contato@fdvweb.com.br', 'Oro Madre 💸')
        .bcc('yuricarvalho48@gmail.com')
        .subject('Solicitação de Nova Senha | Oro Madre 💸');
    });
    console.log('[EVENTO] -> RequestPassword', 'Email enviado com sucesso');
  }
  catch (error) {
    console.error('[EVENTO] -> RequestPassword', error);
  }

});

Event.on('new:Register', async (user_id) => {
  console.log('[EVENTO] -> Register', user_id);
  let user = await UserModel.find(user_id);
  user = user.toJSON();
  let link;

  if (ENV.get('NODE_ENV') === 'development') {
    link = `http://localhost:4200/`;
  }
  else {
    link = `https://painel.oromadre.co/`;
  }

  let email = {
    user: user,
    link: link
  }

  try {
    await Mail.send('emails/welcome', { data: email }, (message) => {
      message
        .to(user.email)
        .from('contato@fdvweb.com.br', 'Oro Madre 💸')
        .bcc('yuricarvalho48@gmail.com')
        .subject('Bem Vindo! | Oro Madre 💸');
    });
    console.log('[EVENTO] -> Register', 'Email enviado com sucesso');
  }
  catch (error) {
    console.error('[EVENTO] -> Register', error);
  }

});


Event.on('new:sendToTelegram', async (analisys_id) => {
  const data = await analisysModel.query().where('id', analisys_id).first();

  const coinPair = await coinPairModel.query().where('id', data.coin_pairs_id).first();


  const currencyType = (data.operation_price_type == 'dolar') ? '*$* ' : '*SATS* ';
  let message = `
*${coinPair.name}* - *${data.market_type}*

_${data.summary}_

*SOBRE A OPERAÇÃO*

📊 *ATIVO:* ${coinPair.name}

${(data.enter_price_in) ? '⬇️ *ENTRADA INICIAL:* ' + currencyType + data.enter_price_in.toFixed(9) : ''}

${(data.enter_price_out) ? '⬇️ *ENTRADA FINAL: * ' + currencyType + data.enter_price_out.toFixed(9) : ''}

${(data.stop_loss) ? '🛑 *STOP LOSS: * ' + currencyType + data.stop_loss.toFixed(9) : ''}

🧨 *CAPITAL COMPROMETIDO*
    * 🔵 CONSERVADOR: * ${data.investiment_risk_initial}
    * 🟡 MODERADO: * ${data.investiment_risk_medium}
    * 🔴 ARROJADO: * ${data.investiment_risk_advance}
`

  if (data.take_profit) data.take_profit = JSON.parse(data.take_profit);

  if (data.take_profit && data.take_profit.length > 0) {
    let _alvos = '';

    data.take_profit.map((item, index) => {
      _alvos = _alvos + ` - 📍 ALVO ${index + 1}: ${currencyType} ${item.value.toFixed(9)}\n`;
    });

    message = message + `\n\n🎯 *ALVOS* \n` + _alvos;

  }

  if (data.leverage) message = message + `\n🚀 *ALAVANCAGEM:* ${data.leverage}X\n`

  if (data.risk_return) message = message + `\n\n⚡ *RISCO RETORNO* ${(data.risk_return)}\n\n`

  message = message + `\n \`Ref: ${data.key}\``

  message = encodeURI(message);

  await TelegramService.sendPhoto({ chatId: TELEGRAM_VIP_GROUP_ID, photo: data.analysis_study_image })
  await TelegramService.sendMessage({ chatId: TELEGRAM_VIP_GROUP_ID, message: message })



  if (data.analysis_permission === 'public') {
    await TelegramService.sendPhoto({ chatId: TELEGRAM_FREE_GROUP_ID, photo: data.analysis_study_image })
    await TelegramService.sendMessage({ chatId: TELEGRAM_FREE_GROUP_ID, message: message })
  }


});

Event.on('new:sendNewsTelegram', async(data) => {

    let message = ``;
    message += `https://t.me/ativnews\n\n`;
    message += `\n ${data.title}`;
    message += `\n\n ${data.link}`;
    message = encodeURI(message);

    await TelegramService.sendPhoto({chatId:TELEGRAM_NEWS_GROUP_ID, photo:data.media, message})

})