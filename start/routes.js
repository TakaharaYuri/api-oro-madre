'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.on('/').render('welcome');
Route.resource('news', 'NewsController');

Route.group(() => {
  Route.post('upload', 'UploadController.uploadImage');
  Route.get('file/:file', 'UploadController.showFile');
  Route.post('login', 'AuthController/Auth.login');
  Route.get('validate-informations', 'AuthController/Auth.validate');
  Route.get('active_plan', 'Manager/PlanController.show');
  Route.post('reset-password', 'AuthController/Auth.resetPassword');
  Route.get('reset-password-validate/:key', 'AuthController/Auth.checkPasswordValidade');
  Route.post('create-password', 'AuthController/Auth.createPassword');
  Route.get('validate-cupom/:cupom', 'Manager/PlanController.getCupom');
}).prefix('api');

Route.group(() => {
  Route.post('credit-card/process', 'PaymentGateway/PagarmeController.process');
  Route.post('pagarme/postback', 'PaymentGateway/PagarmeController.webHookPostback');

}).prefix('api/payment-gateway')

Route.group(() => {
  Route.resource('coin-pairs', 'Manager/CoinPairController');
  Route.resource('users', 'Manager/UserController');
  Route.resource('analysis-manager', 'Manager/AnalysisManagerController');
  Route.get('analysis-manager/send-telegram/:id', 'Manager/AnalysisManagerController.sendToTelegram');
  Route.resource('analysis-room/comments', 'Manager/AnalysisCommentController');
  Route.resource('analysis-room/likes', 'Manager/AnalysisLikeController');
  Route.resource('plans', 'Manager/PlanController');
  Route.resource('clients', 'Manager/ClientController');
  Route.resource('payments', 'Manager/ClientPaymentController');
  Route.resource('cupons', 'Manager/CuponController');
  
  Route.get('analysis-room/analysis', 'Manager/AnalysisManagerController.room');
  Route.get('analysis-room/search', 'Manager/AnalysisManagerController.room');
  Route.get('analysis-room/detail/:id', 'Manager/AnalysisManagerController.detail');
  Route.get('logged-user', 'Manager/UserController.getLoggedUser');
  Route.get('dashboard', 'Manager/DashboardController.index');

  Route.get('pagarme/subscription', 'Manager/PagarmeIntegrationController.getSubscription');
  Route.get('pagarme/cancel-subscription', 'Manager/PagarmeIntegrationController.cancelSubscription');

}).prefix('api').middleware('auth');

